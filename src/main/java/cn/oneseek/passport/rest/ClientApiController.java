package cn.oneseek.passport.rest;

import cn.oneseek.passport.domain.model.ClientEntity;
import cn.oneseek.passport.domain.model.ClientVO;
import cn.oneseek.passport.service.ClientService;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

/**
 * @program: passport-system
 * @description:
 * @author: chuang
 * @create: 2021/1/20 下午5:25
 */
@Path("/api/client")
public class ClientApiController {


    @Inject
    ClientService clientService;
    // 获取client列表
    @Path("/all")
    @GET
    public void getClientList(@QueryParam("username") String username) {
        
    }

    // 获取分页client列表
    @GET
    public void getClientsByPage(String clientId, Long ownerId, Integer page,Integer size) {

    }

    // 创建client
    @POST
    public void createNewClient(ClientVO clientVO) {
        clientService.register(clientVO);
    }

    // 更新client
    @Path("/{clientId}")
    @PUT
    public void updateClientById(Long clientId, ClientEntity client) {
        clientService.updateById(client);
    }

    // 删除client
    @Path("/{clientId}")
    @DELETE
    public void removeClientById(@PathParam("{clientId}") Long clientId) {
        clientService.removeById(clientId);
    }

    // 校验client的合法性
    @Path("/introspect")
    @POST
    public void verifyClient(ClientVO clientVO) {

    }
    
}
