package cn.oneseek.passport.service;

import cn.oneseek.passport.domain.model.ClientEntity;
import cn.oneseek.passport.domain.model.ClientVO;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ClientService {

    public void register(ClientVO clientVO){}
    public void updateById(ClientEntity client){}
    public void removeById(Long clientId){}

}
